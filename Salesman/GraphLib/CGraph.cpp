#include "stdafx.h"
#include <iostream>
#include "CGraph.h"

// =============================================================================
// MyAssert ====================================================================
// =============================================================================

bool MyAssertFunction(const char*strCond, const char *function, const char*filename, int line, int value)
{
	char buf[256];
	const char *valueStr = "Assert no definido";
	switch (value) {
	case AssertTrackSinGrafo: valueStr = "AssertTrackSinGrafo"; break;
	case AssertNoVerticeDelGrafo: valueStr = "AssertNoVerticeDelGrafo"; break;
	case AssertNoHayArista: valueStr = "AssertNoHayArista"; break;
	case AssertDiferenteGrafo: valueStr = "AssertDiferenteGrafo"; break;
	}
	sprintf_s(buf, "ASSERT %s en %s %s (%d). Cond: %s", valueStr, function, filename, line, strCond);
	throw exception(buf);
	return false;
}

// =============================================================================
// CVertex =====================================================================
// =============================================================================

// NeighbordP ===================================================================

bool CVertex::NeighbordP(CVertex *pVertex)
{
	for (list<CVertex*>::const_iterator iter = m_Neighbords.cbegin(); iter != m_Neighbords.cend(); ++iter) {
		if (*iter == pVertex) return true;
	}
	return false;
}

// =============================================================================
// CGraph ======================================================================
// =============================================================================

// CGraph ======================================================================

CGraph::CGraph()
{
	
}


// ~CGraph ====================================================================

CGraph::~CGraph()
{
	m_Vertices.clear();
}

// Clear ======================================================================

void CGraph::Clear()
{
	m_Vertices.clear();
}


// FindVertex ==================================================================

CVertex* CGraph::FindVertex(double x, double y)
{
	for (CVertex &v : m_Vertices) {
		if (v.m_Point.m_X == x && v.m_Point.m_Y == y) return &v;
	}
	return NULL;
}

// GetVertex ===================================================================

CVertex* CGraph::GetVertex(double x, double y)
{
	for (CVertex &v : m_Vertices) {
		if (v.m_Point.m_X == x && v.m_Point.m_Y == y) return &v;
	}
	char msg[256];
	sprintf_s(msg,"CGraph::GetVertex: no se ha encontrado un v�rtice del grafo con coordenadas: %f,%f", x, y);
	throw exception(msg);
}


// GetVertexIndex ==============================================================

int CGraph::GetVertexIndex(const CVertex *pVertex)
{
	int indice = 0;
	for (CVertex &v : m_Vertices) {
		if (&v==pVertex) return indice;
		++indice;
	}
	throw exception("CGraph::GetVertexIndex: no ha encontrado el v�rtice");
}


// GetVertex ===================================================================

CVertex* CGraph::GetVertex(int indice)
{
	int indice0 = indice;
	for (CVertex &v : m_Vertices) {
		if (indice ==0) return &v;
		--indice;
	}
	char msg[256];
	sprintf_s(msg, "CGraph::GetVertex: no se ha encontrado un v�rtice del grafo con indice: %d", indice0);
	throw exception(msg);
}


// MemberP =====================================================================

bool CGraph::MemberP(CVertex *pVertex)
{
	for (CVertex &v : m_Vertices) {
		if (&v==pVertex) return true;
	}
	return false;
}

// GetNVertices ==================================================================

size_t CGraph::GetNVertices()
{
	return m_Vertices.size();
}

// GetNEdges ==================================================================

size_t CGraph::GetNEdges()
{
	size_t n = 0;
	for (CVertex &v : m_Vertices) n+=v.m_Neighbords.size();
	return n / 2;
}

// AddEdge =====================================================================

void CGraph::AddEdge(double x1, double y1, double x2, double y2)
{
	CVertex *pV1 = FindVertex(x1, y1);
	CVertex *pV2 = FindVertex(x2, y2);
	if (pV1 && pV2 && pV1->NeighbordP(pV2)) {
		char msg[256];
		sprintf_s(msg, "CGraph::AddEdge: edge duplicado (%f,%f)-(%f,%f)", x1, y1, x2, y2);
		throw exception(msg);
	}
	if (pV1 == NULL) {
		m_Vertices.push_back(CVertex(x1,y1));
		pV1 = &m_Vertices.back();
	}
	if (pV2 == NULL) {
		m_Vertices.push_back(CVertex(x2, y2));
		pV2 = &m_Vertices.back();
	}
	pV1->m_Neighbords.push_back(pV2);
	pV2->m_Neighbords.push_back(pV1);
}

// Write =======================================================================

void CGraph::Write(const char* filename)
{
	for (CVertex &v : m_Vertices) v.m_Saved = false;
	ofstream  f(filename);
	if (!f.good()) {
		char msg[256];
		sprintf_s(msg, "Error opening graph %s", filename);
		throw exception(msg);
	}
	f << "GRAPH" << endl;
	for (CVertex &v1 : m_Vertices) {
		for (CVertex *pV2 : v1.m_Neighbords) {
			if (!pV2->m_Saved) {
				f << v1.m_Point.m_X << " " << v1.m_Point.m_Y << " " << pV2->m_Point.m_X << " " << pV2->m_Point.m_Y << endl;
			}
		}
		v1.m_Saved = true;
	}
	f.close();
}

// Read ========================================================================

void CGraph::Read(const char* filename)
{
	Clear();
	ifstream  f(filename);
	if (!f.good()) {
		char msg[256];
		sprintf_s(msg, "Error opening graph %s", filename);
		throw exception(msg);
	}
	try {
		char buf[100];
		f.getline(buf,100);
		if (strcmp(buf, "GRAPH") != 0) {
			char msg[256];
			sprintf_s(msg, "CGraph::Read: el fichero no tiene formato de fichero de grafos: %s", filename);
			throw exception(msg);
		}
		// leer v�rtices
		while (!f.eof()) {
			f.getline(buf, 100);
			if (buf[0] == '\0') break;
			double x1, y1, x2, y2;
			sscanf_s(buf, "%lf %lf %lf %lf", &x1, &y1, &x2, &y2);
			//cout << "(" << x1 << "," << y1 << ")-(" << x2 << "," << y2 << ")" << endl;
			AddEdge(x1, y1, x2, y2);
		}
		f.close();
	}
	catch (exception) {
		f.close();
		Clear();
		throw;
	}
}

// WriteDistances ==============================================================

void CGraph::WriteDistances(const char* filename)
{
	ofstream  f(filename);
	if (!f.good()) {
		char msg[256];
		sprintf_s(msg, "Error opening distances %s", filename);
		throw exception(msg);
	}
	f << "DISTANCES" << endl;
	for (CVertex &v1 : m_Vertices) {
		f << fixed << setprecision(2) << v1.m_DijkstraDistance << endl;
	}
	f.close();
}

// ReadDistances ===============================================================

void CGraph::ReadDistances(const char* filename)
{
	ifstream  f(filename);
	if (!f.good()) {
		char msg[256];
		sprintf_s(msg, "Error opening distances %s", filename);
		throw exception(msg);
	}
	try {
		char buf[100];
		f.getline(buf, 100);
		if (strcmp(buf, "DISTANCES") != 0) {
			char msg[256];
			sprintf_s(msg, "CGraph::ReadDistances: el fichero no tiene formato de fichero de distancias: %s", filename);
			throw exception(msg);
		}
		// leer v�rtices
		for (CVertex &v1 : m_Vertices) {
			f.getline(buf, 100);
			if (buf[0] == '\0') {
				char msg[256];
				sprintf_s(msg, "CGraph::ReadDistances: error de lectura de distancias: %s", filename);
				throw exception(msg);
			}
			sscanf_s(buf, "%lf", &v1.m_DijkstraDistance);
		}
	}
	catch (exception) {
		f.close();
		Clear();
		throw;
	}
}

// ReadDistancesAsVector ===============================================================

vector<double> CGraph::ReadDistancesAsVector(const char* filename)
{
	ifstream  f(filename);
	if (!f.good()) {
		char msg[256];
		sprintf_s(msg, "Error opening distances %s", filename);
		throw exception(msg);
	}
	vector<double> distances;
	try {
		char buf[100];
		f.getline(buf, 100);
		if (strcmp(buf, "DISTANCES") != 0) {
			char msg[256];
			sprintf_s(msg, "CGraph::ReadDistances: el fichero no tiene formato de fichero de distancias: %s", filename);
			throw exception(msg);
		}
		// leer v�rtices
		for (CVertex &v1 : m_Vertices) {
			f.getline(buf, 100);
			if (buf[0] == '\0') {
				char msg[256];
				sprintf_s(msg, "CGraph::ReadDistances: error de lectura de distancias: %s", filename);
				throw exception(msg);
			}
			double d;
			sscanf_s(buf, "%lf", &d);
			distances.push_back(d);
		}
	}
	catch (exception) {
		f.close();
		throw;
	}
	return distances;
}

// CrearAleatorio ==============================================================

void CGraph::CrearAleatorio(int nVertices, int nEdges)
{
	Clear();
	srand((unsigned)time(NULL));
	vector<CGPoint> vertices(nVertices);
	for (int i = 0; i<nVertices; ++i) {
		double x, y;
		bool encontrado;
		double minDist = 16000.0 * 16000.0*0.2*0.2;
		do {
			x = rand() % 16000; 
			y = rand() % 16000;
			encontrado = false;
			for (int j = 0; j<i; ++j) {
				double dx = vertices[j].m_X - x;
				double dy = vertices[j].m_Y - y;
				if (dx*dx + dy*dy<minDist) {
					encontrado = true;
					break;
				}
			}
			minDist *= 0.75;
		} while (encontrado);
		vertices[i] = CGPoint(x, y);
		if (i != 0) {
			int j = rand() % i;
			AddEdge(vertices[j].m_X, vertices[j].m_Y, x, y);
			--nEdges;
		}
	}
	int rep = 0;
	while (nEdges>0) {
		int i = rand() % nVertices;
		int j = rand() % nVertices;
		if (i != j && !GetVertex(vertices[i])->NeighbordP(GetVertex(vertices[j]))) {
			AddEdge(vertices[i].m_X, vertices[i].m_Y, vertices[j].m_X, vertices[j].m_Y);
			--nEdges;
			rep = 0;
		}
		else {
			++rep;
			if (rep > 100) throw exception("CGraph::CrearAleatorio: No se puede encontrar un nuevo edge");
		}
	}
}

// TestTrack ====================================================================

bool CGraph::TestTrack(CVisits &visits, CTrack &trackOk, CTrack &trackPrac, ostream &informe)
{
	bool correcto = true;
	if (trackOk.Length() < trackPrac.Length()) {
		correcto = false;
		informe << "Track demasiado largo (" << trackOk.Length() << "," << trackPrac.Length() << ")" << endl;
	}
	if (trackOk.m_Vertices.empty() && !trackPrac.m_Vertices.empty()) {
		correcto = false;
		informe << "El track tiene v�rtices cuando tenia que estar vacio" << endl;
	}
	if (!trackOk.m_Vertices.empty() && trackPrac.m_Vertices.empty()) {
		correcto = false;
		informe << "El track est� vacio" << endl;
	}
	if (trackOk.m_Vertices.empty()!= trackPrac.m_Vertices.empty() ||
		!trackOk.m_Vertices.empty() && !trackPrac.m_Vertices.empty() && trackPrac.m_Vertices.front() != trackOk.m_Vertices.front()) {
		correcto = false;
		informe << "El track no empieza por el primer v�rtice de Visits" << endl;
	}
	for (CVertex *pV : visits.m_Vertices) {
		if (!trackPrac.MemberP(pV)) {
			informe << "El track no contiene el v�rtice de Visits" << pV->m_Point << endl;
		}
	}
	for (CVertex *pV : trackPrac.m_Vertices) {
		if (!MemberP(pV)) {
			correcto = false;
			informe << "El vertice del track no pertenece al grafo:" << pV->m_Point << endl;
		}
	}
	for (list<CVertex*>::const_iterator iter = trackPrac.m_Vertices.cbegin(); iter != trackPrac.m_Vertices.cend();) {
		CVertex *pV1= (*iter);
		++iter;
		if (iter != trackPrac.m_Vertices.cend()) {
			CVertex *pV2 = (*iter);
			if (!pV1->NeighbordP(pV2)) {
				correcto = false;
				informe << "No hay arista para ir del vertice " << pV1->m_Point << " al v�rtice " << pV2->m_Point << endl;
			}
		}
		else break;
	}
	return correcto;
}


// =============================================================================
// CVisits =====================================================================
// =============================================================================

// MemberP =====================================================================

bool CVisits::MemberP(CVertex *pVertex)
{
	for (CVertex* pV : m_Vertices) 	if (pV== pVertex) return true;
	return false;
}

// Write =======================================================================

void CVisits::Write(const char* filename) {
	ofstream  f(filename);
	if (!f.good()) {
		string msg("Error opening visits ");
		msg.append(filename);
		throw exception(msg.c_str());
	}
	f << "VISITS" << endl;
	for (CVertex* pV : m_Vertices) {
		f << pV->m_Point.m_X << " " << pV->m_Point.m_Y << endl;
	}
	f.close();
}

// Read ========================================================================

void CVisits::Read(const char* filename) {
	Clear();
	ifstream  f(filename);
	if (!f.good()) {
		string msg("Error opening visits ");
		msg.append(filename);
		throw exception(msg.c_str());
	}
	try {
		char buf[100];
		f.getline(buf, 100);
		if (strcmp(buf, "VISITS") != 0) {
			char msg[256];
			sprintf_s(msg, "CVisits::Read: el fichero no tiene formato de fichero de visits: %s", filename);
			throw exception(msg);
		}
		// leer v�rtices
		while (!f.eof()) {
			f.getline(buf, 100);
			if (buf[0] == '\0') break;
			double x, y;
			sscanf_s(buf, "%lf %lf", &x, &y);
			Add(m_pGraph->GetVertex(x,y));
		}
		f.close();
	}
	catch (exception) {
		f.close();
		Clear();
		throw;
	}
}

// CrearAleatorio ==============================================================

void CVisits::CrearAleatorio(int nVisits, bool ciclo)
{
	Clear();
	srand((unsigned)time(NULL));
	size_t n = m_pGraph->GetNVertices();
	if (n==0) throw exception("CVisits::CrearAleatorio no puede crear visitas si no hay grafo");

	for (int i = 0; i < nVisits; ++i) {
		CVertex *pVertex;
		int j = 0;
		do {
			int index = rand() % n;
			pVertex = m_pGraph->GetVertex(index);
			++j;
			if (j > 1000) throw exception("CVisits::CrearAleatorio no encuentra un v�rtice diferente a los ya a�adidos");
		} while (MemberP(pVertex));
		Add(pVertex);
	}
	if (ciclo) Add(m_Vertices.front());
}

// << ==========================================================================

ostream& operator<< (ostream& s, const CVisits& visits)
{
	s << "{";
	for (list<CVertex*>::const_iterator iter = visits.m_Vertices.cbegin(); iter != visits.m_Vertices.cend();) {
		const CVertex *pV = *iter;
		s << pV->m_Point;
		++iter;
		if (iter != visits.m_Vertices.cend()) {
			s << ",";
		}
	}
	s << "}";
	return s;
}


// =============================================================================
// CTrack ======================================================================
// =============================================================================

// Append ======================================================================

void CTrack::Append(CTrack &t) 
{
	MyAssert(m_pGraph != NULL, AssertTrackSinGrafo);
	MyAssert(t.m_pGraph != NULL, AssertTrackSinGrafo);
	MyAssert(m_pGraph == t.m_pGraph, AssertDiferenteGrafo);
	MyAssert(m_Vertices.empty() || t.m_Vertices.empty() || m_Vertices.back()->NeighbordP(t.m_Vertices.front()), AssertNoHayArista);
	for (list<CVertex*>::const_iterator iter = t.m_Vertices.cbegin(); iter != t.m_Vertices.cend(); ++iter) {
		m_Vertices.push_back(*iter);
	}
}

// AppendBefore ================================================================

void CTrack::AppendBefore(CTrack &t) 
{
	MyAssert(m_pGraph != NULL, AssertTrackSinGrafo);
	MyAssert(t.m_pGraph != NULL, AssertTrackSinGrafo);
	MyAssert(m_pGraph == t.m_pGraph, AssertDiferenteGrafo);
	MyAssert(m_Vertices.empty() || t.m_Vertices.empty() ||t.m_Vertices.back()->NeighbordP(m_Vertices.front()), AssertNoHayArista);
	for (list<CVertex*>::const_reverse_iterator iter = t.m_Vertices.crbegin(); iter != t.m_Vertices.crend(); ++iter) {
		m_Vertices.push_front(*iter);
	}
}

// Write =======================================================================

void CTrack::Write(const char* filename)
{
	MyAssert(m_pGraph != NULL, AssertTrackSinGrafo);
	ofstream  f(filename);
	if (!f.good()) {
		char msg[256];
		sprintf_s(msg, "Error opening track %s", filename);
		throw exception(msg);
	}
	f << "TRACK" << endl;
	for (list<CVertex*>::const_iterator iter = m_Vertices.cbegin(); iter != m_Vertices.cend(); ++iter) {
		const CGPoint p1 = (*iter)->m_Point;
		f << p1.m_X << " " << p1.m_Y << endl;
	}
	f.close();
}

// Read ========================================================================

void CTrack::Read(const char* filename)
{
	MyAssert(m_pGraph != NULL, AssertTrackSinGrafo);
	Clear();
	ifstream  f(filename);
	if (!f.good()) {
		char msg[256];
		sprintf_s(msg, "Error opening track %s", filename);
		throw exception(msg);
	}
	try {
		char buf[100];
		f.getline(buf, 100);
		if (strcmp(buf, "TRACK") != 0) {
			char msg[256];
			sprintf_s(msg, "CTrack::Read: el fichero no tiene formato de fichero de track: %s", filename);
			throw exception(msg);
		}
		// leer v�rtices
		while (!f.eof()) {
			f.getline(buf, 100);
			if (buf[0] == '\0') break;
			double x, y;
			sscanf_s(buf, "%lf %lf", &x, &y);
			AddLast(m_pGraph->GetVertex(x, y));
		}
		f.close();
	}
	catch (exception) {
		f.close();
		Clear();
		throw;
	}
}

// Length ======================================================================

double CTrack::Length() {
	MyAssert(m_pGraph != NULL, AssertTrackSinGrafo);
	double l = 0.0;
	for (list<CVertex*>::const_iterator iter = m_Vertices.cbegin(); iter != m_Vertices.cend();) {
		const CGPoint p1 = (*iter)->m_Point;
		++iter;
		if (iter != m_Vertices.cend()) {
			const CGPoint p2 = (*iter)->m_Point;
			l += (p2 - p1).Module();
		}
		else break;
	}
	return l;
}

// MemberP =====================================================================

bool CTrack::MemberP(CVertex *pV) {
	MyAssert(m_pGraph != NULL, AssertTrackSinGrafo);
	for (CVertex *pV2 : m_Vertices) {
		if (pV2 == pV) return true;
	}
	return false;
}

// << ==========================================================================

ostream& operator<< (ostream& s, const CTrack& track)
{
	MyAssert(track.m_pGraph != NULL, AssertTrackSinGrafo);
	for (list<CVertex*>::const_iterator iter = track.m_Vertices.cbegin(); iter != track.m_Vertices.cend();) {
		const CVertex *pV = *iter;
		s << pV->m_Point;
		++iter;
		if (iter != track.m_Vertices.cend()) {
			s << "->";;
		}
	}
	return s;
}
