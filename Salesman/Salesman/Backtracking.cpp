#include "stdafx.h"
#include "../GraphLib/CGraph.h"

// SalesmanTrackBacktracking ===================================================

CTrack CGraph::SalesmanTrackBacktracking(CVisits &visits)
{
	CTrack cami = CTrack(this);
	for (CVertex &v : m_Vertices)
	{
		v.m_VertexToVisit = false;
	}

	for (CVertex* v : visits.m_Vertices)
	{
		v->m_VertexToVisit = true;
	}
	costOptim = DBL_MAX;
	CVertex *vertexInici = visits.m_Vertices.front();
	visits.m_Vertices.pop_front();
	cami.AddLast(vertexInici);
	CTrack camiSol = CTrack(this);
	BackTracking(visits, NULL, cami, camiSol, 0);
	return camiSol;
}

CTrack CGraph::BackTracking(CVisits& visits, CVertex* actual, CTrack& cami, CTrack& camiSolucio, double cost)
{
	CVertex *vaux = cami.m_Vertices.back();

	if (visits.m_Vertices.empty())
	{

		if (cost < costOptim)
		{
			costOptim = cost;
			camiSolucio = cami;
		}
		return cami;
	}

	if (cost > costOptim)
		return cami;
	for (CVertex *va : vaux->m_Neighbords)
	{

		bool trobat = false;
		for (CVertex *vb : cami.m_Vertices)
		{
			if (va == vb)
			{
				trobat = true;
			}

		}
		if (!trobat)
		{
			if ((va == visits.m_Vertices.back() && visits.m_Vertices.size() == 1) || va != visits.m_Vertices.back())
			{
				if (va->m_VertexToVisit)
				{
					visits.m_Vertices.remove(va);
				}
				cami.AddLast(va);
				cost = cami.Length();
				BackTracking(visits, vaux, cami, camiSolucio, cami.Length());
				cami.m_Vertices.remove(va);
				if (va->m_VertexToVisit)
					visits.m_Vertices.push_front(va);
				cost = cami.Length();
			}
		}
	}

	if (vaux->m_VertexToVisit && actual != NULL)
	{
		if (actual->m_VertexToVisit)
			visits.m_Vertices.remove(actual);
		cami.AddLast(actual);
		cost = cami.Length();
		BackTracking(visits, vaux, cami, camiSolucio, cami.Length());
		cami.m_Vertices.remove(actual);

		if (actual->m_VertexToVisit)
			visits.m_Vertices.push_front(actual);
		cost = cami.Length();
	}
	return cami;
}



// SalesmanTrackBacktrackingGreedy =============================================

CTrack CGraph::SalesmanTrackBacktrackingGreedy(CVisits &visits)
{
	{
		CTrack camiOptim(this);
		CTrack cami(this);
		list<CVertex *> visitats;
		for (CVertex &v : m_Vertices)
		{
			v.m_Saved = false;
			v.m_VertexToVisit = false;
		}
		for (CVertex *v : visits.m_Vertices)
		{
			visitats.push_back(v);
		}
		costOptim = DBL_MAX;
		cami.AddLast(visits.m_Vertices.front());
		CVertex *auxInicial = visits.m_Vertices.front();
		auxInicial->m_Saved = true;
		CTrack camiSolucio = CTrack(this);
		BackTrackingGreedy(NULL, cami, camiSolucio, visits, visitats, 0);
		return camiSolucio;
	}
}


CTrack CGraph::BackTrackingGreedy(CVertex *vActual, CTrack &cami, CTrack &camiSolucio, CVisits &visits, list<CVertex*> visitats,  double cost)
{

	CVertex *vAux = cami.m_Vertices.back();
	for (CVertex* v : visitats)
	{
		if (v == vAux) visitats.remove(v); break;
	}

	if (visitats.empty())
	{
		if (cost < costOptim)
		{
			costOptim = cost;
			camiSolucio = cami;
		}
		vAux->m_Saved = false;
		return cami;
	}

	for (CVertex *v : visitats)
	{
		if (v != vAux)
		{
			if (!v->m_Saved || (v->m_Saved && visitats.size() == 1))
			{
				CTrack camiAux(this);
				CTrack camiAdd(this);
				DijkstraQueue(vAux);
				camiAux = cami;
				camiAdd.AddFirst(v);
				CVertex *aux = v->m_pDijkstraPrevious;
				while (aux != vAux)
				{
					camiAdd.AddFirst(aux);
					aux = aux->m_pDijkstraPrevious;
				}
				cami.Append(camiAdd);
				cost = cami.Length();
				if (cost <= costOptim)
				{
					BackTrackingGreedy(NULL, cami, camiSolucio, visits, visitats, 0);
				}
				cami = camiAux;
				cost = cami.Length();
			}
		}
	}
	return cami;
}