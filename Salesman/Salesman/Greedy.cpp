#include "stdafx.h"
#include "../GraphLib/CGraph.h"



// SalesmanTrackGreedy =========================================================

CTrack CGraph::SalesmanTrackGreedy(CVisits &visits)
{
	CTrack cami = CTrack(this);
	CVertex* actual = visits.m_Vertices.front();
	CVertex* destiProper = NULL;
	list<CVertex*> visites = visits.m_Vertices;

	visites.pop_front();


	for (int i = 1; i < visits.m_Vertices.size(); i++)
	{
		destiProper = NULL;
		DijkstraQueue(actual);
		for (CVertex* v : visites)
		{
			if (destiProper == NULL) destiProper = v;
			else {
				if (v->m_DijkstraDistance < destiProper->m_DijkstraDistance && visits.m_Vertices.front() != visits.m_Vertices.back()) destiProper = v;
			}
		}
		CTrack camiProvisional = CTrack(this);
		CVertex* camiFet = destiProper;
		while (camiFet != actual)
		{
			camiProvisional.AddFirst(camiFet);
			camiFet = camiFet->m_pDijkstraPrevious;
		}
		if (i == 1) camiProvisional.AddFirst(actual);
		actual = destiProper;
		cami.Append(camiProvisional);
		visites.remove(destiProper);

	}


	return cami;
}


/*for (CVertex* v : visites)
{
	cout << "A la llista: " << v << endl;
}
cout << "___________________________" << endl;*/