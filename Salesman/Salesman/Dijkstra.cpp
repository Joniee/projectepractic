#include "stdafx.h"
#include "../GraphLib/CGraph.h"


struct comparator {
	bool operator()(CVertex* v1, CVertex* v2) {
		return v1->m_DijkstraDistance > v2->m_DijkstraDistance;
	}
};

// =============================================================================
// Dijkstra ====================================================================
// =============================================================================

void CGraph::Dijkstra(CVertex *pStart)
{

	for (CVertex &v : m_Vertices) if (&v == pStart) {
		v.m_DijkstraDistance = 0;
		v.m_DijkstraVisit = false;
	}
	else {
		v.m_DijkstraDistance = DBL_MAX;
		v.m_DijkstraVisit = false;
	}

	list<CVertex*> visitar;
	visitar.push_back(pStart);


	while (!visitar.empty()) {
		for (CVertex *vi : visitar.front()->m_Neighbords) {
			double distance = visitar.front()->m_DijkstraDistance + visitar.front()->m_Point.Distance(vi->m_Point);
			if (vi->m_DijkstraDistance > distance) {
				vi->m_DijkstraDistance = distance;
				vi->m_pDijkstraPrevious = visitar.front();
			}
			if (vi->m_DijkstraVisit == false) visitar.push_back(vi);
		}
		visitar.front()->m_DijkstraVisit = true;
		visitar.pop_front();
	}

}

// =============================================================================
// DijkstraQueue ===============================================================
// =============================================================================

void CGraph::DijkstraQueue(CVertex *pStart)
{
	priority_queue<CVertex*, std::vector<CVertex*>, comparator> queue;

	for (CVertex &v : m_Vertices) {
		if (&v == pStart) {
			v.m_DijkstraDistance = 0;
			v.m_DijkstraVisit = false;
		}
		else {
			v.m_DijkstraDistance = DBL_MAX;
			v.m_DijkstraVisit = false;
		}
	}


	queue.push(pStart);

	while (!queue.empty())
	{
		for (CVertex *vi : queue.top()->m_Neighbords) {
			double distance = queue.top()->m_DijkstraDistance + queue.top()->m_Point.Distance(vi->m_Point);
			if (vi->m_DijkstraDistance > distance) {
				vi->m_DijkstraDistance = distance;
				vi->m_pDijkstraPrevious = queue.top();
			}
			if (vi->m_DijkstraVisit == false) queue.push(vi);
		}
		queue.top()->m_DijkstraVisit = true;
		queue.pop();
	}
	pStart->m_pDijkstraPrevious = NULL;

}
